Maandag 18 september
=======

Vandaag begonnen met feedback over de andere groepjes. Ons spel is goed overgebracht.

- goede uitleg
- goede presentatie
- duidelijke moodboards
- analyse duidelijk 

Ook moesten we ons spel aan de hele klas presenteren doormiddel van een pitch. Jezelf overtuigen waarom jou spel het beste is. 

Nadat we iedereen zn speldoelen hebben aangehoord kregen we de nieuwe debriefing. Weer een nieuw spel maken maar meer gericht op de gekozen opleiding. Ook hebben we enkele opdrachten gekregen wat je met je team moet doen en alleen.

team 
- planning en taakverdeling
- spelanalyse ( observatie ) van 3 bestaande spellen
- verbeterde moodboards nav verdiepend onderzoek
- creatieve technieken toepassen

individueel 
- studiecoach: CMD beroepsprofiel

--------------

> Creatieve Tools (mini workshop)
> 
> - **creatief waarnemen**
> *- - out of the box denken*
> - **uitstel van oordeel**
> *- -  pas ordelen in de convergerende fase*
> - **flexibel associeren**
> *-  - verschillende technieken*
> - **divergeren**
> *-  - veel ideeen generegen*
> - **verbeeldingskracht**
> *- -  het vermogen om te visualiseren*

Na een beetje overlegt te hebben wie wat meeneemt woensdag om het spel te 
onderzoeken zijn we begonnen met de workshop blog en zijn we nu echt begonnen 
met het schrijven van een blog op de ' echte site gitlab '. 
Het is even wennen om het allemaal op deze site op te schrijven maar als 
je het eenmaal door hebt lukt het prima.
