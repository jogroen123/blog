Maandag 11 september
====================

Vandaag echt begonnen aan het brainstormen over het spel dat we gaan maken. We zijn het er over uit dat het een bord spel wordt
en het thema heeft vooral te maken met de metro. Het wordt een twee delen bordspel: op de ene de metro plattegrond en de andere de stad rotterdam
met alle hots spots. Ook nagedacht welke kleuren het moet krijgen en een tekening bijgemaakt. Volgende les gaan we beginnen met het maken van een prototype
en gaan we het testen of het ook echt werkt en hoelang het spel duurt.

Verder hebben we deze les teamafspraken gemaakt waar we ons aan moeten houden.

> - http://bjoziasse.giftlab.io <--- deze link moeten we even onthouden.
